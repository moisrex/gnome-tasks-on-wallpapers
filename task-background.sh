#!/bin/bash

PID=$(pgrep gnome-session)
export DBUS_SESSION_BUS_ADDRESS=$(grep -z DBUS_SESSION_BUS_ADDRESS /proc/$PID/environ|cut -d= -f2-)

background="$HOME/Pictures/Wallpapers/arch.jpg";
#secs=$($(($(date +%s) - $(date +%s -r "$old"))));

#if [ "$(task ids)" != "$(cat /tmp/tasks 2>/dev/null)" ]; then
  
  old=$(gsettings get org.gnome.desktop.background picture-uri | tr -d "'")
  #old=$(dconf read /org/gnome/desktop/background/picture-uri | tr -d "'");
  url=$(mktemp --suffix=.jpg)

  convert "$background" -font FreeMono -fill white -pointsize 20 -gravity east -annotate +100+0 "$(task list)" "${url}";

  echo old: $old
  echo new: $url

  gsettings set org.gnome.desktop.background picture-uri "'file://${url}'";
  #sudo -u moisrex dbus-launch --exit-with-session dconf write /org/gnome/desktop/background/picture-uri "'file://${url}'";

  if [[ "$old" == "file:///tmp/"* ]]; then
    rm -f ${old/file:\/\//};
  fi;

  #task ids > /tmp/tasks;

#fi;
exit;

